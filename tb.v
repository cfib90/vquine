`timescale 1ns/1ps
module tb();

    reg clk = 1'b0;
    wire tx;
    
    q dut(clk,tx);
    
    always
        #41.66 clk <= ~clk;
        
    initial begin
        $dumpfile("vquinesim.vcd");
        $dumpvars;
        repeat (2000000) @(posedge clk);
        $finish;
    end
    reg samp = 1'b0;
    reg ne = 1'b0;
    integer i;
    reg [7:0] sr;
    integer fp;
    initial begin
        fp = $fopen("vquinesimout.v","w");
        while (1) begin
            while(tx != 1'b1) @(posedge clk);
            ne <= 1'b1;
            while(tx != 1'b0) @(posedge clk);
            ne <= 1'b0;
            #2170;
            samp <= 1'b1;
            for (i = 0; i < 8; i=i+1) begin
                #4340;
                sr <= {tx,sr[7:1]};
                samp <= 1'b0;
            end
            #4340;
            $fwrite(fp,"%c",sr);
            if (tx != 1) $display("Stop Bit Error");
        end
        $fclose(fp);
    end

endmodule
