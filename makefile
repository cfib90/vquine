PORT?=/dev/ttyUSB1

.PHONY:simcheck hwcheck all

all: simcheck hwcheck

vquinefsm.bin : vquinefsm.asc
	icepack vquinefsm.asc vquinefsm.bin

vquinefsm.json : vquinefsm.v
	yosys -p 'synth_ice40 -json vquinefsm.json' vquinefsm.v
	
vquinefsm.asc : vquinefsm.json
	nextpnr-ice40 --package sg48 --up5k --json vquinefsm.json --asc vquinefsm.asc --freq 12 --pcf pins.pcf
	
	
simcheck : vquinesimout.v vquinefsm.v
	diff -s $^
	
vquinesimout.txt : vquinefsm.exe
	vvp $<
	
vquinefsm.exe : vquinefsm.v tb.v
	iverilog $^ -o $@
	
hwcheck : vquinefsm.bin vquinefsm.v
	stty -F $(PORT) speed 230400
	head -n 1 $(PORT) | tee vquinehwout.v & iceprog -S $<; wait
	diff -s vquinehwout.v vquinefsm.v

clean : 
	rm -f vquinefsm.bin vquinefsm.exe vquinehwout.v vquinesimout.txt vquinesim.vcd vquinefsm.json
