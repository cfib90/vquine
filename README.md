# Synthesizable Verilog Quine #

This is my take on the "Hardware Quine" problem:

*A chip design that outputs its own HDL source!*

See https://codegolf.stackexchange.com/q/99676 for further info.

## Getting Started ##

The design should be able to run on any FPGA.

In the current state it is tuned to run on the iCEBreaker board (Lattice iCE40 UP5K).

* It runs on the iCEBreaker's 12 MHz clock frequency
* It outputs to asynchronous serial, 230400 8N1

To start a test of the quine in simulation, run

    make swcheck
    
To start a test of the quine in hardware, run

    make hwcheck
    
This requires exactly one (1) iCEBreaker board to be connected to the PC.

The makefile has a `PORT` parameter to specify the serial port if it is not at `/dev/ttyUSB1`.

Both checks can be run using

    make all

## About the Design ##

The design can be found in `vquinefsm.v`

Nothing really that special or clever, just a "data" and a "printer" portion.

It takes up about 500 LCs on iCE40, so there's definitely room for improvement.

However, compared to the smaller (130 LEs) solution here, https://codegolf.stackexchange.com/a/100114, this solution...

* can be used with common serial ports and
* does not require any pre-processing (a special DEC SIXBIT encoding is used in the smaller solution).

My solution, synthesized with yosys using `synth_intel -family cycloneiv` uses 534 "cells". 

## It works! ##

<script id="asciicast-zUmHyyZRJ9yEzqHLsLoaZjjpC" src="https://asciinema.org/a/zUmHyyZRJ9yEzqHLsLoaZjjpC.js" async></script>

(Link: https://asciinema.org/a/zUmHyyZRJ9yEzqHLsLoaZjjpC)
